<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<div class="edfooter">
      <ul>
        <li>Contato e Suporte</li>
        <li>Alunos: prg@usp.br</li>
            <li>Docentes e Monitores: suporte@edisciplinas.usp.br</li>
        <li>Telefone: +55 11 2648-0037</li>
      </ul>
    </div>



<?php wp_footer(); ?>

</body>
</html>
