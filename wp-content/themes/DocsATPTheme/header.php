<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<link rel="stylesheet" type="text/css" media="(max-width:967px)" href="<?php echo get_stylesheet_directory_uri(). '/phone.css' ?>">

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header" role="banner">


    	<div class="edlogos">
    		<img src="<?php echo get_theme_file_uri('media/usp-logo.png') ?>" align="left" id="edusplogo">
    		<p id="logo1">Universidade de São Paulo</p>
    		<p id="logo2">Documentação Moodles da USP</p>
    		<p id="logo3">Apoio aos Moodles da USP</p>
    	</div>


    <div id="page" class="site">
    	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

    	<div class="edbar">
    		<div class="edpinkline"></div>
	    		<div class="edbarcontent">
						<div class="main-navigation">
	          <?php
	    				wp_nav_menu(array(
	    					'theme_location' => 'headerMenuLocation'
	    				));
	    			?>
	          <li id="edbarimg"><img src="<?php echo get_theme_file_uri('media/logo_edisciplinas.png') ?>"></li>
	    		</div>
				</div>
    	</div>
    </div>

	</header><!-- #masthead -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
