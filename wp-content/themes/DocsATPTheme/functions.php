<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

    $parent_style = 'twentyseventeen'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'DocsATPTheme',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('1.0.0')
    );
}

function change_posts_order($query) {
  if($query->is_home() && $query->is_main_query()) { // edit to match the desired page
     $query->set('orderby', 'name');
  }
}
add_action('pre_get_posts', 'change_posts_order');

function site_features() {
  register_nav_menu('headerMenuLocation','Header Menu Location');
}

add_action('after_setup_theme', 'site_features');

?>
