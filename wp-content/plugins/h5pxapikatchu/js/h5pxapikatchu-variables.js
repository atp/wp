// Set environment variables
window.H5PxAPIkatchu = {
	captureAllH5pContentTypes: h5pxapikatchu.captureAllH5pContentTypes,
	debugEnabled: h5pxapikatchu.debugEnabled,
	h5pContentTypes: h5pxapikatchu.h5pContentTypes,
	jQuery: jQuery,
	wpAJAXurl: h5pxapikatchu.wpAJAXurl
};
