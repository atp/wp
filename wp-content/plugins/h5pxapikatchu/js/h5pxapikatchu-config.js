// Set environment variables
window.H5PxAPIkatchu = {
  captureAllH5pContentTypes: '1',
  debugEnabled: '1',
  h5pContentTypes: [ '' ],
  jQuery: H5P.jQuery,
  wpAJAXurl: 'https://atp.usp.br/wp-admin/admin-ajax.php'
};
